terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.50"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  region="us-east-1"
}

resource "aws_vpc" "MerquriVPC"{
   cidr_block = "10.192.0.0/16"
   tags = {
	Name = "Merquri Main VPC"
   }

}

resource "aws_subnet" "public"{
    vpc_id = aws_vpc.MerquriVPC.id
    cidr_block = "10.192.0.0/24"
    availability_zone = "us-east-1a"
    tags = {
      Name = "Mequri Public Subnet"
    }
}


resource "aws_subnet" "private"{
    vpc_id = aws_vpc.MerquriVPC.id
    cidr_block = "10.192.1.0/24"
    availability_zone = "us-east-1a"
    tags = {
      Name = "Mequri Private Subnet"
    }
}

resource "aws_internet_gateway" "igw"{
    vpc_id=aws_vpc.MerquriVPC.id
    tags={
      Name="MerquriIGW"
    }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.MerquriVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "Mequri Public IGB Table"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.MerquriVPC.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
    
  }
  tags = {
    Name = "Mequri Private NAT Table"
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}
