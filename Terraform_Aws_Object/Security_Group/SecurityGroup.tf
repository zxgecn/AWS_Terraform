terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.50"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_security_group" "MerquriSecurityGroup" {
  name        = "MerquriSecurtiyGroup_TCP80_TCP22"
  description = "Allow TCP 22 and 80"
  vpc_id      = "vpc-0aaf12c6cf74e5363"

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
   #cidr_blocks      = ["118.189.0.0/16","116.206.0.0/16","223.25.0.0/16"]
    cidr_blocks      = ["0.0.0.0/0"]  
}

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

ingress {
    description      = "Ping from VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "icmp"
    cidr_blocks      = ["0.0.0.0/0"]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Tags_Allow_HTTP80_SSH22"
  }
}
