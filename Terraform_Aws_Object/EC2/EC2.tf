terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.50"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}
data "aws_ami" "ZxgecnAMI" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
resource "aws_instance" "NgnixHost" {
  ami           = data.aws_ami.ZxgecnAMI.id
  instance_type = "t2.micro"
  key_name = "Zxgecn_SSH_Key"
  vpc_security_group_ids  = ["sg-0eb81e44f268ae588"]
  subnet_id = "subnet-03498123f86e663a6"
  tags = {
    Name = "Ngnix HOST Server"
  }
}

