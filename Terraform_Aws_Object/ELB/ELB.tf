terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.50"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_elb" "MerquriELB" {
  name    = "MerquriELB"
  subnets = ["subnet-01539b571518e353c"]
  security_groups = ["sg-0eb81e44f268ae588"]
 
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 22
    instance_protocol  = "tcp"
    lb_port            = 22
    lb_protocol        = "tcp"
  }
  
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:22/"
    interval            = 30
  }

  instances                   = ["i-02f1e9f2e076b76e3"]
  cross_zone_load_balancing   = true
  idle_timeout                = 600
  connection_draining         = true
  connection_draining_timeout = 300

  tags = {
    Name = "MerquriELB"
  }
}
